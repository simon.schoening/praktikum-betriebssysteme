//! Memory allocation APIs

use crate::{arch::syscall, syscall::Syscall};
use core::alloc::Layout;

/// Allocate memory from the operating system.
///
/// See [`alloc::alloc::alloc`](alloc::alloc::alloc).
///
/// # Safety
///
/// See [`GlobalAlloc::alloc`](alloc::alloc::GlobalAlloc::alloc).
pub(crate) unsafe fn alloc(layout: Layout) -> *mut u8 {
	syscall::syscall2(Syscall::Alloc as _, layout.size(), layout.align()) as _
}

/// Deallocate memory from the operating system.
///
/// See [`alloc::alloc::dealloc`](alloc::alloc::dealloc).
///
/// # Safety
///
/// See [`GlobalAlloc::dealloc`](alloc::alloc::GlobalAlloc::dealloc).
pub(crate) unsafe fn dealloc(ptr: *mut u8, layout: Layout) {
	todo!("implement")
}

#[cfg(test)]
mod tests {
	use super::*;
	use alloc::alloc::{alloc, dealloc};

	#[test]
	fn test_edu_libstd_alloc() {
		let layout = Layout::new::<[u64; 128]>();
		unsafe {
			let newmem = alloc(layout);
			dealloc(newmem, layout);
		}
	}
}
