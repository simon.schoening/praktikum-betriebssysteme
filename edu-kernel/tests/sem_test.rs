//! This test checks if a semaphore can be used for synchronization between
//! threads

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

#[no_mangle]
static FORCE_LINK: u8 = 0;

use alloc::sync::Arc;
use alloc::vec::Vec;
use core::sync::atomic::{AtomicUsize, Ordering};
use edu_kernel::sync::semaphore::Semaphore;
use edu_kernel::{arch::delay, println, scheduler::thread};

static COUNT: AtomicUsize = AtomicUsize::new(0);

fn post_thread(sem: Arc<Semaphore>) {
	for i in 0..6 {
		delay(10000000);
		println!("post nr {}", i);
		COUNT.fetch_add(1, Ordering::SeqCst);
		sem.post();
	}
}

fn wait_thread(sem: Arc<Semaphore>, id: usize) {
	println!("Wait Thread {} started", id);
	for _ in 0..3 {
		sem.wait();
		println!("Wait Thread {} got semaphore", id);
	}
	// if the post thread hasn't had three posts yet, something went wrong
	assert!(COUNT.load(Ordering::SeqCst) >= 3);
}

fn basic_test() {
	let sem = Semaphore::new(0);
	sem.post();
	sem.wait();
}

/// This function is run by the kernel after initialization.
#[no_mangle]
pub extern "C" fn _init() {
	basic_test();

	// We use an Arc here, to provide access to the Semaphore in multiple threads
	let sem: Arc<Semaphore> = Arc::new(Semaphore::new(0));
	let mut threads = Vec::new();

	threads.push({
		let sem = sem.clone();
		thread::spawn(|| wait_thread(sem, 1))
	});
	threads.push({
		let sem = sem.clone();
		thread::spawn(|| wait_thread(sem, 2))
	});
	threads.push({
		let sem = sem.clone();
		thread::spawn(|| post_thread(sem))
	});

	// wait for all threads to finish
	for t in threads {
		t.join();
	}
}
