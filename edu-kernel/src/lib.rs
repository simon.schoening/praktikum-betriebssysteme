#![no_std] // Removes std from the prelude.

// Unstable feature gates
#![feature(abi_x86_interrupt)]
#![feature(alloc_error_handler)]
#![feature(asm)]
#![feature(const_fn)]
#![feature(const_fn_fn_ptr_basics)]
#![feature(const_panic)]
#![feature(int_bits_const)]
#![feature(linkage)]
#![feature(naked_functions)]
#![feature(new_uninit)]
#![feature(option_unwrap_none)]
#![allow(incomplete_features)]
#![feature(specialization)]
// Test configuration
#![cfg_attr(test, no_main)] // #[no_main] attribute in test configuration
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]
#![reexport_test_harness_main = "test_main"]
// Lint configuration
#![deny(broken_intra_doc_links)]
#![allow(clippy::missing_safety_doc)]
#![deny(rust_2018_idioms)]

extern crate alloc;

#[cfg(test)]
#[macro_use]
extern crate edu_testmacro;

#[macro_use]
extern crate derive_more;

#[macro_use]
pub mod console;

pub mod addr;
pub mod arch;
pub mod consts;
pub mod logger;
pub mod mm;
pub mod scheduler;
pub mod sync;
pub mod syscall;

mod once;

use console::Color;
use core::panic::PanicInfo;
use log::info;

/// Initializes the kernel systems. Must be called only once before the
/// application starts. This function is called from the architecture specific
/// boot code (e.g. in `arch/x86_64/kernel/start.rs`)
pub unsafe fn edu_kernel_init() {
	//unsafe { asm!("cli") }
	logger::init().unwrap();
	info!("Initializing kernel");

	arch::init();
	mm::init();
	scheduler::init();

	// enable interrupts => enable preemptive multitasking
	arch::interrupts::enable();
	arch::system_timer::start();
}

/// This function is called on panic.
#[panic_handler]
pub fn panic(info: &PanicInfo<'_>) -> ! {
	// #[panic_handler] is a global attribute. We would like to redefine it in
	// libstd for userspace panics, but as long as we compile kernel and libstd
	// together, we cannot. This is why we multiplex here. If we are not in
	// kernelspace we call the userspace panic handler from libstd.
	if arch::in_kernelspace() {
		arch::interrupts::disable();
		println!(
			"{reset}{red}[PANIC]{reset} {info}",
			reset = Color::Reset,
			red = Color::RedFG,
			info = info,
		);
		arch::processor::exit(1)
	} else {
		/// This handler is meant to be overwritten by libstd at link-time. We
		/// have to provide a fallback though, in which case we throw an
		/// undefined instruction exception.
		#[linkage = "weak"]
		#[no_mangle]
		fn libstd_panic(_info: &PanicInfo<'_>) -> ! {
			unsafe { asm!("ud2", options(noreturn)) }
		}

		libstd_panic(info)
	}
}

#[cfg(test)]
#[test]
fn kerneltest() {
	println!("-- Kernel Unit test");
}

/// Simple test framework. Runs all functions annotated with `[test-case]`
pub fn test_runner(tests: &[&dyn Fn()]) {
	println!();
	println!("======== edu-kernel Test Framework =========");
	println!("Running {} tests", tests.len());
	println!();
	for test in tests {
		test();
		println!();
	}
	println!("===== All tests successfully completed =====");
	println!();
}
