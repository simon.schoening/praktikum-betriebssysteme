// Copyright (c) 2017 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

//! Configuration parameter of the kernel eduOS-rs

use crate::mm::freelist::AllocationStrategy;

/// Define the size of the kernel stack
pub const STACK_SIZE: usize = 0x100000;

/// Size of a cache line
pub const CACHE_LINE: usize = 64;

/// Maximum number of priorities
pub const NR_PRIORITIES: u8 = 32;

/// frequency of the timer interrupt
pub const TIMER_FREQ: u32 = 100; /* in HZ */

/// Entry point of the user tasks
pub const USER_ENTRY: usize = 0x8000000000usize;

pub const ALLOCATION_STRATEGY: AllocationStrategy = AllocationStrategy::FirstFit;
