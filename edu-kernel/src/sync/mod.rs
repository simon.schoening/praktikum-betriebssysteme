//! Synchronization primitives
//!
//! The structure of mutex types is based on [`lock_api`](https://docs.rs/lock_api)
//! and the implementations are based on [`spin`](https://docs.rs/spin).

pub mod interrupt;
pub mod semaphore;
pub mod spinlock;

use core::cell::UnsafeCell;

/// Basic operations for a mutex.
///
/// Types implementing this trait can be used by [`Mutex`] to form a safe mutex
/// type. Implementors should also define a corresponding [`Mutex`]-type for
/// clarity.
///
/// # Safety
///
/// Implementations of this trait must ensure that the mutex is actually
/// exclusive: a lock can't be acquired while the mutex is already locked.
pub unsafe trait RawMutex {
	/// Initial value for an unlocked mutex.
	// Can hopefully be replaced with `const fn new() -> Self` at some point.
	#[allow(clippy::declare_interior_mutable_const)]
	const INIT: Self;

	// Acquires this mutex, blocking the current thread until it is able to do so.
	fn lock(&self);

	/// Unlocks this mutex.
	///
	/// # Safety
	///
	/// This method may only be called if the mutex is held in the current
	/// context, i.e. it must be paired with a call to [`lock`](Self::lock).
	unsafe fn unlock(&self);
}

/// A mutual exclusion primitive.
///
/// Exclusive access to the inner value is provided via [`Mutex::lock`].
///
/// This design allows different synchronization implementations, as it consists
/// of two parts:
///
/// - `raw`: The synchronization method used for this Mutex.
/// - `data`: The data which should be protected.
///
/// This type should not be referred to by users. Instead implementors of
/// [`RawMutex`] define a corresponding [`Mutex`]-type that should be used
/// instead.
#[derive(Debug, Default)]
pub struct Mutex<RAW: RawMutex, T: ?Sized> {
	/// The [`RawMutex`] used for locking and unlocking.
	raw: RAW,
	/// The protected data.
	///
	/// [`UnsafeCell`] is used to get a `&mut T` from `&self` once we ensured
	/// exclusivity manually at runtime.
	data: UnsafeCell<T>,
}

unsafe impl<RAW: RawMutex + Send, T: ?Sized + Send> Send for Mutex<RAW, T> {}
unsafe impl<RAW: RawMutex + Sync, T: ?Sized + Send> Sync for Mutex<RAW, T> {}

impl<RAW: RawMutex, T> Mutex<RAW, T> {
	/// Creates a new mutex.
	pub const fn new(value: T) -> Self {
		Self {
			raw: RAW::INIT,
			data: UnsafeCell::new(value),
		}
	}

	/// Unwraps the inner value.
	pub fn into_inner(self) -> T {
		self.data.into_inner()
	}
}

impl<RAW: RawMutex, T: ?Sized> Mutex<RAW, T> {
	/// Returns a mutable reference to the inner value.
	pub fn get_mut(&mut self) -> &mut T {
		// Having `&mut self` guarantees exclusivity at compile-time.
		unsafe { &mut *self.data.get() }
	}

	/// Locks the mutex and runs the provided closure on the inner value.
	///
	/// # Example
	///
	/// ```
	/// let mutex = Mutex::new(0);
	///
	/// // Access the inner value
	/// let read = mutex.lock(|data| {
	///     *data = 2;
	///     *data
	/// });
	///
	/// assert_eq!(2, read);
	/// ```
	pub fn lock<RET>(&self, f: impl FnOnce(&mut T) -> RET) -> RET {
		self.raw.lock();

		let ret = f(unsafe { &mut *self.data.get() });

		unsafe { self.raw.unlock() };

		ret
	}

	/// Forcibly unlocks the mutex.
	///
	/// Use with caution!
	pub unsafe fn force_unlock(&self) {
		self.raw.unlock()
	}

	/// Returns a raw pointer to the underlying data.
	///
	/// Use with caution!
	pub fn data_ptr(&self) -> *mut T {
		self.data.get()
	}
}

impl<RAW: RawMutex, T> From<T> for Mutex<RAW, T> {
	fn from(value: T) -> Self {
		Self::new(value)
	}
}
